﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using System.Text;
using System;

public class SceneManeger02a : MonoBehaviour
{

    public Canvas cv01InitCallibration, cv02CallibratingNow, cv03CallibrateFinished;

    //Variable for callibration
    private Touch touch;
    private string filePathCalibration;
    private float calibratedDriftSpeed = 0.0f;

    public float calibrationTime = 5;
    public GvrViewer GvrMain;
    public Transform head;
    public float driftSpeed = 0;
    public bool isCallibrate;
    public float samplingTime = 0.2f;
    public float relYAngle;
    public float multiply = 1.03f;

    public Text TextLine2;

    float rotY;
    float elapsed;
    long iter = 0;

    private bool isTracked;


    // Use this for initialization
    void Start()
    {

        cv01InitCallibration.enabled = true;
        cv02CallibratingNow.enabled = false;
        cv03CallibrateFinished.enabled = false;

        filePathCalibration = Application.persistentDataPath + "/ " + "calibration.dat";
        if (File.Exists(filePathCalibration))
        {

            Debug.Log("Calibration data is found.");
            calibratedDriftSpeed = LoadCalibData();

        }
        else
        {

            Debug.Log("No calibration data.");
            calibratedDriftSpeed = 0.0f;

        }

        isCallibrate = true;
        TextLine2.GetComponent<Text>().text = "";

        Invoke("StartCallibrate", 1.0f);

    }

    void Update()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        elapsed += Time.fixedDeltaTime;

        Quaternion relative = Quaternion.Inverse(transform.rotation) * head.transform.rotation;
        relYAngle = relative.eulerAngles[1];

        if (relYAngle > 180)
        {
            relYAngle -= 360;
        }


        // proceed with calibration
        if (elapsed < calibrationTime && isCallibrate == true)
        {

            TextLine2.GetComponent<Text>().text = Mathf.Round(calibrationTime - elapsed).ToString() + "秒間静止してお待ちください。";

        }
        else if (isCallibrate == true)
        {

            //this is the most important line, in which we obtain the drifting speed
            driftSpeed = relYAngle / calibrationTime;
            Debug.Log("Rotating speed is:" + driftSpeed);

        }


        // Save drift speed information to file
        if (elapsed > calibrationTime && isCallibrate == true)
        {
            TextLine2.GetComponent<Text>().text = "";


            Debug.Log("Save calibration data.");
            SaveCalibData(driftSpeed);
            Debug.Log("Correction");

            isCallibrate = false;

            Debug.Log("Calibration data read again.");
            calibratedDriftSpeed = LoadCalibData();

            cv02CallibratingNow.enabled = false;
            cv03CallibrateFinished.enabled = true;

            Invoke("MoveToStartMenu", 3.0f);

        }



        // apply drift speed correction from file
        if (isCallibrate == false)
        {

            GvrMain.transform.rotation = GvrMain.transform.rotation * Quaternion.Euler(0, -calibratedDriftSpeed * Time.fixedDeltaTime * multiply, 0);

        }


    }

    public void MoveToStartMenu()
    {
        Debug.Log("Move to Start Menu");
        SceneManager.LoadScene("00_StartMenu");
    }

    public void StartCallibrate()
    {
        cv01InitCallibration.enabled = false;
        cv02CallibratingNow.enabled = true;
        cv03CallibrateFinished.enabled = false;

        driftSpeed = 0;
        iter = 0;
        elapsed = 0;
        rotY = 0;
        isCallibrate = true;

        transform.rotation = Quaternion.Euler(0, head.rotation.eulerAngles[1], 0);

        Debug.Log("Start to callibrate");
        //goInfoText.GetComponent<Text>().text = "キャリブレーション開始";

    }

    public void SaveCalibData(float driftSpeed)
    {
        Debug.Log("Drift Speed is " + driftSpeed.ToString());
        StreamWriter sw;
        sw = new StreamWriter(filePathCalibration, false);
        sw.WriteLine(driftSpeed.ToString());
        sw.Close();

    }

    public float LoadCalibData()
    {
        FileInfo fi = new FileInfo(filePathCalibration);
        float returnSt = 0.0f;

        try
        {
            using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8))
            {
                returnSt = float.Parse(sr.ReadToEnd());
                Debug.Log("calibration.dat:" + returnSt);
            }
        }
        catch (Exception e)
        {
            print(e.Message);
            returnSt = 0.0f;
        }

        return returnSt;
    }



}
