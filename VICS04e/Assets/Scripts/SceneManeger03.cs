﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManeger03 : MonoBehaviour
{

    const string COUNT_START_KEY = "movieStart";
    const string COUNT_FINISH_KEY = "movieFinish";

    public Canvas cv01CountView, cv02ResetConfirm;

    public Text Text_CountStart;
    public Text Text_CountFinish;

    public int cntS, cntF;

    // Use this for initialization
    void Start()
    {
        cv01CountView.enabled = true;
        cv02ResetConfirm.enabled = false;

        
        cntS = PlayerPrefs.GetInt(COUNT_START_KEY, 0);
        cntF = PlayerPrefs.GetInt(COUNT_FINISH_KEY, 0);

        Debug.Log("Start: " + cntS.ToString() + ",    Finish: " + cntF.ToString());

        Text_CountStart.text = cntS.ToString();
        Text_CountFinish.text = cntF.ToString();


    }

    void Update()
    {

        /*
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            int cnt = PlayerPrefs.GetInt(COUNT_START_KEY, 0);
            PlayerPrefs.SetInt(COUNT_START_KEY, cnt + 1);
            PlayerPrefs.Save();

            Text_CountStart.text = PlayerPrefs.GetInt(COUNT_START_KEY, 0).ToString();
            Text_CountFinish.text = PlayerPrefs.GetInt(COUNT_FINISH_KEY, 0).ToString();

        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            int cnt = PlayerPrefs.GetInt(COUNT_FINISH_KEY, 0);
            PlayerPrefs.SetInt(COUNT_FINISH_KEY, cnt + 1);
            PlayerPrefs.Save();

            Text_CountStart.text = PlayerPrefs.GetInt(COUNT_START_KEY, 0).ToString();
            Text_CountFinish.text = PlayerPrefs.GetInt(COUNT_FINISH_KEY, 0).ToString();
        }
        */
    }

    public void MoveToStartMenu()
    {
        Debug.Log("Move to Start Menu");
        SceneManager.LoadScene("00_StartMenu");
    }

    public void ResetConfirm()
    {
        cv01CountView.enabled = false;
        cv02ResetConfirm.enabled = true;
    }

    public void Reset_No()
    {
        cv01CountView.enabled = true;
        cv02ResetConfirm.enabled = false;
    }

    public void Reset_Yes()
    {

        PlayerPrefs.SetInt(COUNT_START_KEY, 0);
        PlayerPrefs.SetInt(COUNT_FINISH_KEY, 0);
        PlayerPrefs.Save();

        Text_CountStart.text = PlayerPrefs.GetInt(COUNT_START_KEY, 0).ToString();
        Text_CountFinish.text = PlayerPrefs.GetInt(COUNT_FINISH_KEY, 0).ToString();

        cv01CountView.enabled = true;
        cv02ResetConfirm.enabled = false;
        
    }

}
