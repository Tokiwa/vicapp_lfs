﻿using UnityEngine;
using UnityEngine.SceneManagement;
using RenderHeads.Media.AVProVideo;
using System.Collections;


public class SceneManager01 : MonoBehaviour
{

    public float elapsed;
    public MediaPlayer MPLayer;

    private bool isDoubleTapStart;
    private float doubleTapTime;

    public GameObject goHead;

    public bool buttomHide = true;
    public GameObject goButtom;
    public Material matButtom;
    public float aplphaButtom;

    public bool naviHide = true;
    public GameObject goNavi;
    public Material matNavi;
    public float aplphaNavi;

    public float speed = 0.01f;

    public float trackOn;
    public bool isTracked = false;

    public float buttomOn;
    public float naviOn;
    public float naviOff;

    public float doublTapInterval = 0.2f;

    const string COUNT_START_KEY = "movieStart";
    const string COUNT_FINISH_KEY = "movieFinish";

    // Use this for initialization
    void Start()
    {
        /*
        goHead.GetComponent<GvrHead>().trackRotation = false;
        goHead.GetComponent<GvrHead>().trackPosition = false;
        */

        matButtom = goButtom.GetComponent<Renderer>().material;
        matNavi = goNavi.GetComponent<Renderer>().material;

        matButtom.color = new Color(1, 1, 1, 0);
        matNavi.color = new Color(1, 1, 1, 0);

        MPLayer.Events.AddListener(OnVideoEvent);

    }

    //「コルーチン」で呼び出すメソッド
    IEnumerator MoveToMenu()
    {
        yield return new WaitForSeconds(0.5f);  //0.5秒待つ
        Debug.Log("Now start page");

        SceneManager.LoadScene("00_StartMenu");

    }

    // FixedUpdate is called every time
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R)){
            SceneManager.LoadScene("00_StartMenu");
        }
    }


        // FixedUpdate is called every time
    void FixedUpdate()
    {
        // double tap でメニューに戻る処理
        if (isDoubleTapStart)
        {
            doubleTapTime += Time.deltaTime;
            if (doubleTapTime < doublTapInterval)
            {
                if (Input.GetMouseButtonDown(0) && Input.touchCount == 1)
                {
                    Debug.Log("double tap");

                    isDoubleTapStart = false;
                    doubleTapTime = 0.0f;
                    StartCoroutine("MoveToMenu");

                }
            }
            else
            {
                Debug.Log("reset");
                // reset
                isDoubleTapStart = false;
                doubleTapTime = 0.0f;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("down");
                isDoubleTapStart = true;
            }
        }

        elapsed += Time.fixedDeltaTime;
        //Debug.Log("Elapsed" + elapsed.ToString());

        if (elapsed > buttomOn && buttomHide == true)
        {

            aplphaButtom += speed;
            if (aplphaButtom < 1)
            {
                matButtom.color = new Color(1, 1, 1, aplphaButtom);
            }
            else if (aplphaButtom > 1)
            {
                aplphaButtom = 1;
                buttomHide = false;
            }

        }

        if (elapsed > naviOn && naviHide == true)
        {

            aplphaNavi += speed;
            if (aplphaNavi < 1)
            {
                matNavi.color = new Color(1, 1, 1, aplphaNavi);
            }
            else
            {
                aplphaNavi = 1;
                naviHide = false;
            }

        }

        if (elapsed > naviOff && naviHide == false)
        {
            aplphaNavi -= speed;
            if (aplphaNavi > 0)
            {

                matNavi.color = new Color(1, 1, 1, aplphaNavi);

            }
            else
            {

                aplphaNavi = 0;
                //naviHide = true;

            }


            aplphaButtom -= speed;
            if (aplphaButtom > 0 && buttomHide == false)
            {

                matButtom.color = new Color(1, 1, 1, aplphaButtom);

            }
            else
            {

                aplphaButtom = 0;
                //buttomHide = true;

            }

        }


    }

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.MetaDataReady:
                Debug.Log("MediaPlayerEvent:MetaDataReady");
                break;

            case MediaPlayerEvent.EventType.ReadyToPlay:
                Debug.Log("MediaPlayerEvent:ReadyToPlay");
                //mp.Control.Play();
                break;
            case MediaPlayerEvent.EventType.FirstFrameReady:
                Debug.Log("MediaPlayerEvent:FirstFrameReady");
                mp.Control.SetTextureProperties(FilterMode.Bilinear, TextureWrapMode.Repeat, 0); //Offset指定時に正常に表示されるように
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                Debug.Log("MediaPlayerEvent:FinishedPlaying");
                int cntF = PlayerPrefs.GetInt(COUNT_FINISH_KEY);

                PlayerPrefs.SetInt(COUNT_FINISH_KEY, cntF + 1);
                PlayerPrefs.Save();
                SceneManager.LoadScene("00_StartMenu");
                break;
            case MediaPlayerEvent.EventType.Started:
                Debug.Log("MediaPlayerEvent:Started");
                int cntS = PlayerPrefs.GetInt(COUNT_START_KEY);
                PlayerPrefs.SetInt(COUNT_START_KEY, cntS + 1);
                PlayerPrefs.Save();
                break;
        }
        Debug.Log("Event:" + et.ToString());
    }

    void CountUp(string Key)
    {
        Debug.Log("Count up:" + Key);
        int cnt = PlayerPrefs.GetInt(Key, 0);
        PlayerPrefs.SetInt(Key, cnt + 1);
        PlayerPrefs.Save();
    }

}
