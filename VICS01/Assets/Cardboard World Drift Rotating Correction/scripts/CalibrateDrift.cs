﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System;
using RenderHeads.Media.AVProVideo;

public class CalibrateDrift : MonoBehaviour {

    private Touch touch;
    private string filePathCalibration;
    private float calibratedDriftSpeed = 0.0f;

    public float calibrationTime=5;
	public GvrViewer CB;
	public Transform head;
	public float driftSpeed=0;
	public bool calib;
	public float samplingTime=0.2f;
	public float relYAngle;
    public float multiply = 1.01f;

    float rotY;
	float elapsed;
	long iter=0;
    
    public GameObject SceneManager;
    private bool isTracked;

    // Use this for initialization
    void Start () 
	{
       
        filePathCalibration = Application.persistentDataPath + "/ " + "calibration.dat";
        if (File.Exists(filePathCalibration))
        {

            Debug.Log("Calibration data is found.");
            calibratedDriftSpeed = LoadCalibData();

        }
        else
        {

            Debug.Log("No calibration data.");
            calibratedDriftSpeed = 0.0f;

        }


    }
	
	// Update is called once per frame
	void FixedUpdate () 
	{

        elapsed +=Time.fixedDeltaTime;

		Quaternion relative = Quaternion.Inverse(transform.rotation) * head.transform.rotation;
		relYAngle = relative.eulerAngles[1]; 

		if(relYAngle >180)
		{
			relYAngle-=360;
		}


        // if pressed reset calibration process
        /*
        if (Input.touchCount == 3)
        {
			driftSpeed=0;
			iter=0;
			elapsed=0;
			//elapsed2=0;
			rotY=0;
			calib=true;

			transform.rotation=Quaternion.Euler(0,head.rotation.eulerAngles[1],0);

            goInfoText.GetComponent<Text>().text = "キャリブレーション開始";

        }
        */


		// proceed with calibration
		if(elapsed < calibrationTime && calib == true )
		{
            

        }
		else if( calib == true)
		{

			//this is the most important line, in which we obtain the drifting speed
			driftSpeed = relYAngle / calibrationTime;

            Debug.Log("Rotating speed is:" + driftSpeed);

        }


        // Save drift speed information to file
        if (elapsed > calibrationTime && calib == true)
		{

            Debug.Log("Save calibration data.");
            SaveCalibData(driftSpeed);
			Debug.Log("Correction");

            calib = false;

            Debug.Log("Calibration data read again.");
            calibratedDriftSpeed = LoadCalibData();

            
        }

        // apply drift speed correction from file
        if (calib == false)
        {

            CB.transform.rotation = CB.transform.rotation * Quaternion.Euler(0, -calibratedDriftSpeed * Time.fixedDeltaTime * multiply, 0);

        }


    }

    public void SaveCalibData(float driftSpeed)
    {
        
        StreamWriter sw;
        sw = new StreamWriter(filePathCalibration, false);
        sw.WriteLine(driftSpeed.ToString());
        sw.Close();

    }

    public float LoadCalibData()
    {
        FileInfo fi = new FileInfo(filePathCalibration);
        float returnSt = 0.0f;

        try
        {
            using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8))
            {
                returnSt = float.Parse(sr.ReadToEnd());
                Debug.Log("calibration.dat:" + returnSt);
            }
        }
        catch (Exception e)
        {
            print(e.Message);
            returnSt = 0.0f;
        }

        return returnSt;
    }


}
