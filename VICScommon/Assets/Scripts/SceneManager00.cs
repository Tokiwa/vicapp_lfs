﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager00 : MonoBehaviour
{

    private bool isDoubleTapStart;
    private float doubleTapTime;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        
    }

    public void MoveToMain()
    {
        Debug.Log("Move to Main scene");
        SceneManager.LoadScene("01_Main");
    }

    public void MoveToCallibrate()
    {
        Debug.Log("Move to Callibrate scene");
        SceneManager.LoadScene("02_Callibrate");
    }

    public void MoveToCounter()
    {
        Debug.Log("Move to Counter scene");
        SceneManager.LoadScene("03_Counter");
    }

}